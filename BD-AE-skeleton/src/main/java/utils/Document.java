package utils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Document implements Serializable{
	
	private static final long serialVersionUID = 123L;
	
	
	
	int documentID;
	String title;
	Map<String, Long> words;
	int numberOfWords;
	
	public Document(int documentID, String title, Map<String, Long> words, int numberOfWords) {
		super();
		this.documentID = documentID;
		this.title = title;
		this.words = words;
		this.numberOfWords = numberOfWords;
	}
	
	public int getDocumentID() {
		return documentID;
	}

	public void setDocumentID(int documentID) {
		this.documentID = documentID;
	}

	public int getNumberOfWords() {
		return numberOfWords;
	}

	public void setNumberOfWords(int numberOfWords) {
		this.numberOfWords = numberOfWords;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Map<String, Long> getWords() {
		return words;
	}

	public void setWords(Map<String, Long> words) {
		this.words = words;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numberOfWords;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((words == null) ? 0 : words.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Document other = (Document) obj;
		if (numberOfWords != other.numberOfWords)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (words == null) {
			if (other.words != null)
				return false;
		} else if (!words.equals(other.words))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[title=" + title +", numberOfWords="
				+ numberOfWords + "]";
	}

}
