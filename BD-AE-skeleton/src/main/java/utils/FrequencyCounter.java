package utils;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FrequencyCounter {
	
	public static Map<String, Long> countFrequency(String text) {
	    List<String> stopWordsList = new ArrayList<String>();
	    stopWordsList.add("you");
		return Stream.of(text)
	            .map(s -> s.split(" "))
	            .flatMap(Arrays::stream)
	            .map(FrequencyCounter::clean) 
	            .map(s -> s.toLowerCase())
	            .map(s -> new PorterStemmer().stem(s))
	            .filter(s -> !s.equals(""))
	            .filter(s -> !stopWordsList.contains(s))
	            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
	}
	
	public static String clean(String word) {
        return word.replaceAll("[1234567—890!@#$%^&*()_+|\\-=~`{}\\[\\]:;\"<>,.?/]", "").toLowerCase(); 
    }
	
	public static Long sumFrequencies(Map<String, Long> map) {
		Long sum = map.values()
				  .stream()
				  .mapToLong(Long::valueOf)
				  .sum();
		return sum;
	}
	
	
	public static void main(String[] args) {
		
		String text = "Hello, Here, how are.% you, you ok, are you university?";
		
		Map<String, Long> map = countFrequency(text);
		Long sum = sumFrequencies(map);
		
		System.out.println("Map: " + map.toString());
		System.out.println("Number of words:" + sum.toString());
	}
}
