import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.hadoop.conf.Configuration;
import utils.PorterStemmer;
import java.util.*;
import java.util.stream.Collectors;

import scala.Tuple2;


public class MyIndexer {

	public static void main(String[] args) throws ParseException {
		
		String inputFile = "";
		String outputDir = "";
		
		if (args.length != 2) {
			System.out.println("Incorrect command: Should have 2 arguments");
			System.out.println("Expected command: <input> <output>");
			System.exit(1);
		} else {
			inputFile = args[0];
	        outputDir = args[1];
		}
       
		// Set Spark Context and Configuration
		JavaSparkContext sc = new JavaSparkContext(new SparkConf().setAppName("MyPageRank").setMaster("local[2]"));

		// Set delimiter for stopwords
		sc.hadoopConfiguration().set("textinputformat.record.delimiter", "\n");
		// Load Stopwords
		String stopWordsPath = "src/main/resources/stopword-list.txt";
		List<String> stopWordsList = sc.textFile(stopWordsPath).collect();
		sc.broadcast(stopWordsList); // broadcast (make read-only and shared with all executors
		System.out.println(stopWordsList.size());
		
		// Set delimiter for Wikipedia articles to be /n[[
		Configuration conf = new Configuration();
		conf.set("textinputformat.record.delimiter", "\n[[");
		sc.hadoopConfiguration().set("textinputformat.record.delimiter", "\n[[");
		
        // Load text data
        JavaRDD<String> inputData = sc.textFile(inputFile);

        JavaPairRDD <String, String> RDD = inputData.mapToPair(s -> new Tuple2<String, String>(s, extractTitle(s)));

        JavaPairRDD <String, Tuple2<String, Integer>> words = RDD.flatMapToPair(
        		s -> {
					ArrayList<Tuple2<String, Tuple2<String, Integer>>> output = new ArrayList<>();
					List<String> wordList = Arrays.asList(s._1().split(" ")).stream()
							.map(word -> clean(word))
							.map(word -> word.trim())
							.map(word -> word.toLowerCase())
							.filter(word -> !stopWordsList.contains(word))
							.map(word -> new PorterStemmer().stem(word))
							.collect(Collectors.toList());

					HashMap<String, Integer> counts = new HashMap<String, Integer>();
					for (int i = 0; i < wordList.size(); i++) {
						if (counts.containsKey(wordList.get(i))) {
							counts.replace(wordList.get(i), counts.get(wordList.get(i)) + 1);
						} else {
							counts.put(wordList.get(i), 1);
						}
					}
					for (String key : counts.keySet()) {
						output.add(
								new Tuple2<String, Tuple2<String, Integer>>(
										key, new Tuple2<String, Integer>(s._2(),counts.get(key))));
					}
					return output.iterator();
				}
		);

        JavaPairRDD <String, Integer> wordCount = RDD.mapToPair(s -> {
			List<String> wordList = Arrays.asList(s._1().split(" ")).stream()
					.map(word -> clean(word))
					.map(word -> word.trim())
					.map(word -> word.toLowerCase())
					.filter(word -> !stopWordsList.contains(word))
					.map(word -> new PorterStemmer().stem(word))
					.collect(Collectors.toList());
			return new Tuple2<String, Integer>(s._2(), wordList.size());
		});

        words.mapToPair(w -> new Tuple2<>(w._2()._2(), new Tuple2<> (w._1(), w._2()._1()))).sortByKey(false)
				.mapToPair(w -> new Tuple2<>(w._2()._1(), new Tuple2<>(w._2()._2(), w._1())))
				.groupByKey()
				.sortByKey()
				.saveAsTextFile(outputDir + "/postingsList");

        wordCount.sortByKey().saveAsTextFile(outputDir + "/wordCount");

        sc.close();
		
	}
	
	public static String extractTitle(String articleText) {
		String title = null;	
		title = articleText.split("]]")[0].replace("[[", "");
		title = title.trim();
		return(title);
	}

	public static String clean(String word) {
		return word.replaceAll("[1234567—890!@#$%^&*()_+|\\-=~`{}\\[\\]:;\"<>,.?/]", "").toLowerCase();
	}

}
