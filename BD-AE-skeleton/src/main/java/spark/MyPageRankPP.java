package spark;
import java.text.ParseException;

import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.yarn.server.webproxy.ProxyUtils._;

import utils.FrequencyCounter;
import utils.PorterStemmer;
import utils.Document;
import utils.FrequencyCounter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import scala.Tuple2;


public class MyPageRankPP {
	
	static int documentCounter = 0;
	static int totalWords = 0;
	static int averageDL = 0;

	public static void main(String[] args) throws ParseException {
		
		String inputFile = "";
		String outputDir = "";
		
		if (args.length != 2) {
			System.out.println("Incorrect command: Should have 2 arguments");
			System.out.println("Expected command: <input> <output>");
			System.exit(1);
		} else {
			inputFile = args[0];
	        outputDir = args[1];
		}
       
		// Set Spark Context and Configuration
		JavaSparkContext sc = new JavaSparkContext(new SparkConf().setAppName("MyPageRank").setMaster("local[2]"));
		
		// Set delimiter for stopwords
		sc.hadoopConfiguration().set("textinputformat.record.delimiter", "\n");
		
		// Load Stopwords
		String stopWordsPath = "src/main/resources/stopword-list.txt";
		List<String> stopWordsList = sc.textFile(stopWordsPath).collect();
		sc.broadcast(stopWordsList); // broadcast (make read-only and shared with all executors
		System.out.println("Stopwords: " + stopWordsList.size());
		sc.broadcast(documentCounter);
		sc.broadcast(totalWords);
		
		// Set delimiter for Wikipedia articles to be /n[[
		Configuration conf = new Configuration();
		conf.set("textinputformat.record.delimiter", "\n[[");
		sc.hadoopConfiguration().set("textinputformat.record.delimiter", "\n[[");
        
        JavaRDD<Document> RDD = sc.textFile(inputFile)
            	.flatMap(d -> Arrays.asList(d.split("\n\\[\\[")).iterator())
            	.map(doc -> {
            		
            		int documentID = documentCounter++;
            		String title = extractTitle(doc);
            		Map<String, Long> words = countFrequency(doc, stopWordsList);
            		int numberOfWords = Math.toIntExact(sumFrequencies(words));
            		
            		Document document = new Document(documentID, title, words, numberOfWords);
            		totalWords += numberOfWords;
            		
            		return (document);
            	});
        
        
        
        JavaPairRDD<String,Integer> DocumentToLength = RDD
        		.mapToPair(d -> {
        			return new Tuple2<String,Integer>(d.getTitle(),d.getNumberOfWords());
        		});
        
        
		/*
		 * JavaPairRDD<String,List<Tuple2<String,Integer>>> postingsList = RDD
		 * .flatMapToPair(d -> d.getWords().entrySet() .stream() .map(term -> new
		 * Tuple<String,Integer> (term,d.getWords().get(term))).iterator()
		 * 
		 * .reduceByKey((v1, v2) -> v1 + v2).collect();;
		 */
        
        //System.out.println(inputData.collect());
        //System.out.println(inputData.count());  
        //System.out.println("Documents count: " + sc.textFile(inputFile).count());
		//System.out.println("RDD Count: " + RDD.count()); 
		//System.out.println("Document Counter: " + documentCounter);
		//System.out.println("Total Document Words: " + totalWords);
		//averageDL = totalWords / documentCounter;
		//System.out.println("averageDL: " + averageDL);
        
        int numberOfDocuments = Math.toIntExact(RDD.count());
      	JavaRDD<Integer> wordCounts = RDD.map(d -> d.getNumberOfWords());
        int totalWords = wordCounts.reduce((a, b) -> a + b);
        int averageDL = totalWords / numberOfDocuments;
        
        //RDD.saveAsTextFile(outputDir);
        
        System.out.println("Number of Documents: " + numberOfDocuments);
        System.out.println("Total Document Words: " + totalWords);
        System.out.println("Average Document Length: " + averageDL);
        
        for(Tuple2<String,Integer> line:DocumentToLength.collect()){
            System.out.println("* "+line);
        }
        
        sc.close();
		
	}
	
	public static String extractTitle(String articleText) {
		String title = null;	
		title = articleText.split("]]")[0].replace("[[", "");
		title = title.trim();
		return(title);
	}
	
	public static Map<String, Long> countFrequency(String text, List<String> swList) {
		return Stream.of(text)
	            .map(s -> s.split(" "))
	            .flatMap(Arrays::stream)
	            .map(FrequencyCounter::clean)
	            .map(s -> new PorterStemmer().stem(s))
	            .filter(s -> !s.equals(""))
	            .filter(s -> !swList.contains(s))
	            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
	}
	
	public static String clean(String word) {
        return word.replaceAll("[1234567—890!@#$%^&*()_+|\\-=~`{}\\[\\]:;\"<>,.?/]", "").toLowerCase(); 
    }
	
	public static Long sumFrequencies(Map<String, Long> map) {
		Long sum = map.values()
				  .stream()
				  .mapToLong(Long::valueOf)
				  .sum();
		return sum;
	}
	

}
